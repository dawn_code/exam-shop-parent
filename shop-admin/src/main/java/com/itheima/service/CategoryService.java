package com.itheima.service;


import com.itheima.dto.CategoryPageQueryDto;
import com.itheima.pojo.Category;
import com.itheima.result.PageResult;
import com.itheima.vo.CategoryChildrenItemVo;

import java.util.List;

public interface CategoryService {

    /**
     * 新增分类
     */
    void save(Category category);

    /**
     * 修改分类
     */
    void updateById(Category category);

    /**
     * 删除分类
     */
    void deleteById(Integer id);

    /**
     * 根据ID查询
     */
    Category getById(Integer id);

    /**
     * 分页查询
     */
    PageResult page(CategoryPageQueryDto pageQueryDto);

    /**
     * 更新显示状态
     */
    void updateNavStatus(Integer id, Integer status);

    /**
     * 根据parentId查询分类列表
     */
    List<Category> list(Integer parentId);

    /**
     * 查询全部
     */
    List<Category> findAll();

    /**
     * 查询分类列表及其下面的子分类数据
     */
    List<CategoryChildrenItemVo> findCategoryListWithChildren();
}
