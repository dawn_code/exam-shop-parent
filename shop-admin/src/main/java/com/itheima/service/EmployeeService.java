package com.itheima.service;


import com.itheima.dto.EmployeePageQueryDto;
import com.itheima.pojo.Employee;
import com.itheima.result.PageResult;

public interface EmployeeService {

    /**
     * 新增员工
     */
    void save(Employee employee);

    /**
     * 分页查询
     */
    PageResult page(EmployeePageQueryDto pageQueryDTO);

    /**
     * 启用/禁用员工
     */
    void enableOrDisable(Integer status, Integer id);

    /**
     * 根据ID查询员工信息
     */
    Employee getById(Integer id);

    /**
     * 更新员工信息
     */
    void update(Employee employee);
}
