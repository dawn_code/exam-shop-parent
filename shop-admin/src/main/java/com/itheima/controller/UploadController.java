package com.itheima.controller;

import com.itheima.result.Result;
import com.itheima.utils.AliOssUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.UUID;

@Slf4j
@Api(tags = "文件上传")
@RestController
public class UploadController {

    @Autowired
    private AliOssUtil aliOssUtil;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file) throws IOException {
        log.info("文件上传 , file: {}" , file.getName());

        //获取文件的拓展名
        String originalFilename = file.getOriginalFilename();
        String extname = originalFilename.substring(originalFilename.lastIndexOf("."));

        //组装新的文件名 , 上传文件
        String fileName = UUID.randomUUID().toString() + extname;
        String url = aliOssUtil.upload(file.getBytes(), fileName);

        //响应数据
        return Result.success(url);
    }

}
