package com.itheima.controller;

import com.itheima.dto.BrandPageQueryDto;
import com.itheima.pojo.Brand;
import com.itheima.result.PageResult;
import com.itheima.result.Result;
import com.itheima.service.BrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 品牌管理
 */
@Slf4j
@Api(tags = "品牌管理")
@RestController
@RequestMapping("/shop/brands")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 新增品牌
     */
    @ApiOperation("新增品牌")
    @PostMapping
    public Result save(@RequestBody Brand brand) {
        brandService.save(brand);
        return Result.success();
    }

    /**
     * 根据ID更新品牌
     */
    @ApiOperation("更新品牌")
    @PutMapping
    public Result updateById(@RequestBody Brand brand) {
        brandService.updateById(brand);
        return Result.success();
    }

    /**
     * 根据ID删除品牌
     */
    @ApiOperation("删除品牌")
    @DeleteMapping("/{id}")
    public Result deleteById(@PathVariable Integer id) {
        brandService.deleteById(id);
        return Result.success();
    }

    /**
     * 根据ID查询品牌
     */
    @ApiOperation("根据ID查询品牌")
    @GetMapping("/{id}")
    public Result<Brand> getById(@PathVariable Integer id) {
        Brand brand = brandService.getById(id);
        return Result.success(brand);
    }

    /**
     * 分页查询
     */
    //请求限流
    @ApiOperation("分页查询")
    @GetMapping
    public Result<PageResult> page(BrandPageQueryDto pageQueryDto){
        PageResult pageResult = brandService.page(pageQueryDto);
        return Result.success(pageResult);
    }

    /**
     * 查询全部
     */
    @ApiOperation("查询全部品牌")
    @GetMapping("/all")
    public Result<List<Brand>> findAll(){
        List<Brand> brandList = brandService.findAll();
        return Result.success(brandList);
    }
}
