package com.itheima.mapper;

import com.itheima.dto.ProductPageQueryDto;
import com.itheima.pojo.Product;
import com.itheima.vo.ProductVo;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ProductMapper {

    /**
     * 条件查询
     */
    List<ProductVo> list(ProductPageQueryDto pageQueryDto);

    /**
     * 新增商品
     */
    void insert(Product product);

    /**
     * 更新商品
     */
    void update(Product product);

    /**
     * 根据ID查询商品
     */
    Product getById(Integer id);

    /**
     * 根据ID删除商品
     */
    void deleteById(Integer id);

}
