package com.itheima.mapper;

import com.itheima.dto.EmployeePageQueryDto;
import com.itheima.pojo.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    /**
     * 新增员工
     */
    @Insert("insert into tb_employee (username, real_name, password, email, note, status, create_time, update_time, create_user, update_user) " +
            "values (#{username}, #{realName},  #{password},  #{email}, #{note}, #{status}, #{createTime}, #{updateTime}, #{createUser}, #{updateUser})")
    void insert(Employee employee);

    /**
     * 动态条件查询
     */
    List<Employee> list(EmployeePageQueryDto pageQueryDTO);

    /**
     * 更新员工
     */
    void update(Employee employee);

    //根据ID查询员工信息
    @Select("select id, username, real_name, password, email, note, status, create_time, update_time, create_user, update_user from tb_employee where id = #{id}")
    Employee getById(Integer id);
}
