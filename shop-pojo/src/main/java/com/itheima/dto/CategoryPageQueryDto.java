package com.itheima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CategoryPageQueryDto {

    private Integer page; //页码
    private Integer pageSize; //每页展示记录数
    private String name; //分类名称
    private Integer navStatus; //是否显式在导航栏 , 0: 不显示, 1: 显式
    private Integer parentId;//父分类ID

}
