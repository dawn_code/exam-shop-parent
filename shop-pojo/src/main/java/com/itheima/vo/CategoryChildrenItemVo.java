package com.itheima.vo;

import com.itheima.pojo.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryChildrenItemVo extends Category{
    //子分类
    private List<Category> children;
}
