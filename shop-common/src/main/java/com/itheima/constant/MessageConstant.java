package com.itheima.constant;

/**
 * 信息提示常量类
 */
public class MessageConstant {

    public static final String PASSWORD_ERROR = "密码错误";
    public static final String ACCOUNT_NOT_FOUND = "账号不存在";
    public static final String ACCOUNT_LOCKED = "账号被锁定";
    public static final String UNKNOWN_ERROR = "未知错误";
    public static final String USER_NOT_LOGIN = "用户未登录";
    public static final String LOGIN_FAILED = "登录失败";
    public static final String UPLOAD_FAILED = "文件上传失败";
    public static final String LOGIN_LOCK_ERROR_MESSAGE = "您的账号已经被锁定";
    public static final String DATA_TRANSFER_ERROR = "数据转换异常";
    public static final String CATEGORY_NOT_DELETED = "商品分类下有子分类, 不可以删除";
    public static final String PRODUCT_PUBLISH_CANNOT_DELETE = "上架中的商品, 不可以直接删除";
    public static final String REQUEST_RATE_LIMIT_MESSAGE = "服务接口访问频率过高，请稍后重试。";



}
