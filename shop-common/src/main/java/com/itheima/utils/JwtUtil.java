package com.itheima.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * JWT令牌操作工具类
 */
public class JwtUtil {

    private static final String SECREY_KEY = "SVRIRUlNQQ==";

    /**
     * 生成jwt
     */
    public static String createJWT(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, SECREY_KEY.getBytes(StandardCharsets.UTF_8)).compact();
    }

    /**
     * jwt解析
     */
    public static Claims parseJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECREY_KEY.getBytes(StandardCharsets.UTF_8))
                .parseClaimsJws(token).getBody();
        return claims;
    }

}
